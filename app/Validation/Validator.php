<?php

namespace App\Validation;

use Slim\Container;
use Psr\Http\Message\ServerRequestInterface as Request;
use Respect\Validation\Exceptions\NestedValidationException;

class Validator
{
    protected $container;
    protected $errors;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->errors = [];
    }

    /**
     * Iterates through each validation rule and checks if passed or not.
     * Adding exception messages into session should a rule fail.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param array $rules
     * @return $this
     */
    public function validate(Request $request, array $rules)
    {
        foreach ($rules as $field => $rule) {
            if ($rule == 'recaptcha') {
                $recaptcha = $this->container->recaptcha->verify(
                    $request->getParam($field), 
                    $request->getServerParam('REMOTE_ADDR')
                );

                if (!$recaptcha->isSuccess()) {
                    $this->errors['recaptcha'] = 'ReCaptcha validation failed';
                }
            } else {
                try {
                    $rule->setName(ucfirst($field))->assert($request->getParam($field));
                } catch (NestedValidationException $e) {
                    $this->errors[$field] = $e->getMessages();
                }
            }
        }

        $_SESSION['errors'] = $this->errors;

        return $this;
    }

    /**
     * Checks if any validation errors have been returned.
     *
     * @return bool
     */
    public function failed()
    {
        return !empty($this->errors);
    }
}