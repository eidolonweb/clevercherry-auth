<?php

namespace App\Validation\Rules;

use Doctrine\Common\Inflector\Inflector;
use Respect\Validation\Rules\AbstractRule;

class Unique extends AbstractRule
{
    protected $table;
    protected $field;

    public function __construct($rule)
    {
        $rule = explode('.', $rule);

        $this->table = $rule[0];
        $this->field = $rule[1];
    }

    /**
     * Checks if the value already exists in the table specified or not.
     *
     * @param $input
     * @return bool
     */
    public function validate($input)
    {
        $model = '\\App\\Models\\' . ucfirst(Inflector::singularize($this->table));

        return $model::where($this->field, $input)->count() === 0;
    }
}
