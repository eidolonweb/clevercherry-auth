<?php

namespace App\Validation\Rules;

use Respect\Validation\Rules\AbstractRule;
use Psr\Http\Message\ServerRequestInterface as Request;

class Matches extends AbstractRule
{
    protected $request;
    protected $field;

    public function __construct(Request $request, $field)
    {
        $this->request = $request;
        $this->field = $field;
    }

    /**
     * Checks if the values of two fields match.
     *
     * @param $input
     * @return bool
     */
    public function validate($input)
    {
        return $input === $this->request->getParam($this->field);
    }
}
