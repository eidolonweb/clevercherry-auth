<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = [
        'name', 'email', 'password', 'active', 'group_id',
    ];

    protected $hidden = [
        'password',
    ];

    /**
     * Checks if user has administrator access
     *
     * @return bool
     */
    public function administratorAccess()
    {
        return $this->role_id === 1;
    }

    /**
     * Checks if user has moderator access
     *
     * @return bool
     */
    public function moderatorAccess()
    {
        return $this->role_id <= 2;
    }

    /**
     * Checks if user needs to activate their account
     *
     * @return bool
     */
    public function needsToActivate()
    {
        return !$this->active && $this->activationToken;
    }

    /**
     * Role 'belongs to' user relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    /**
     * User 'has one' activation token relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function activationToken()
    {
        return $this->hasOne(ActivationToken::class);
    }
}
