<?php

namespace App\Controllers;

use App\Models\User;
use App\Models\ActivationToken;
use App\Controllers\Controller;
use Respect\Validation\Validator as v;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class AuthController extends Controller
{
    /**
     * Render login view
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Message\ResponseInterface $response
     * @return mixed
     */
    public function getLogin(Request $request, Response $response)
    {
        return $this->view->render($response, 'auth/login.twig');
    }

    /**
     * Process user login
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Message\ResponseInterface $response
     * @return mixed
     */
    public function postLogin(Request $request, Response $response)
    {
        $_SESSION['old'] = $request->getParams();

        $validation = $this->validator->validate($request, [
            'email' => v::notEmpty()->noWhitespace()->length(null, 255)->email(),
            'password' => v::notEmpty()->noWhitespace()->length(null, 255),
            'g-recaptcha-response' => 'recaptcha',
        ]);

        if ($validation->failed()) {
            return $response->withRedirect($this->router->pathFor('auth.login'));
        }

        $user = $this->auth->attempt($request->getParam('email'), $request->getParam('password'));

        if (!$user) {
            $this->flash->addMessage('flash', [
                'message' => 'We could not log you in with those details',
                'alert' => 'danger',
            ]);
            
            return $response->withRedirect($this->router->pathFor('auth.login'));
        }

        if ($user->needsToActivate()) {
            $this->flash->addMessage('flash', [
                'message' => 'Your account has not yet been activated. Please log into your email account and click the activation link.',
            ]);

            $this->auth->logout();
            
            return $response->withRedirect($this->router->pathFor('auth.login'));
        }        

        if (!$user->active) {
            $this->flash->addMessage('flash', [
                'message' => 'Your account is no longer active. Please contact Clevercherry to arrange for this account to be reactivated.',
            ]);

            $this->auth->logout();
            
            return $response->withRedirect($this->router->pathFor('auth.login'));
        }        

        return $response->withRedirect($this->router->pathFor('home'));
    }

    /**
     * Render register view
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Message\ResponseInterface $response
     * @return mixed
     */
    public function getRegister(Request $request, Response $response)
    {
        return $this->view->render($response, 'auth/register.twig');
    }

    /**
     * Process user registration
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Message\ResponseInterface $response
     * @return mixed
     */
    public function postRegister(Request $request, Response $response)
    {
        $validation = $this->validator->validate($request, [
            'name' => v::notEmpty()->length(null, 255)->alpha(),
            'email' => v::notEmpty()->noWhitespace()->length(null, 255)->email()->unique('users.email'),
            'password' => v::notEmpty()->noWhitespace()->length(6, 255),
            'password_confirm' => v::notEmpty()->matches($request, 'password'),
            'g-recaptcha-response' => 'recaptcha',
        ]);

        if ($validation->failed()) {
            $_SESSION['old'] = $request->getParams();

            return $response->withRedirect($this->router->pathFor('auth.register'));
        }

        $user = User::create([
            'name' => $request->getParam('name'),
            'email' => $request->getParam('email'),
            'password' => password_hash($request->getParam('password'), PASSWORD_DEFAULT),
        ]);

        $token = $this->random->getRandomString(128);

        $user->activationToken()->create([
            'token' => $token,
        ]);

        $this->mail->isSMTP();
        $this->mail->Host = $this->settings['mail']['host'];
        $this->mail->SMTPAuth = true;
        $this->mail->Username = $this->settings['mail']['user'];
        $this->mail->Password = $this->settings['mail']['password'];
        $this->mail->SMTPSecure = $this->settings['mail']['encryption'];
        $this->mail->Port = $this->settings['mail']['port'];

        $this->mail->setFrom('hello@clevercherry.com', 'Clevercherry');
        $this->mail->addAddress($request->getParam('email'), $request->getParam('name'));
        $this->mail->isHTML(true);

        $this->mail->Subject = 'Clevercherry Account Activation';
        $this->mail->Body = $this->view->fetch('emails/activation.twig', [
            'url' => $this->request->getUri()->getBaseUrl() . $this->router->pathFor('auth.activate', ['token' => $token]),
        ]); 

        $this->mail->send();        
        
        $this->flash->addMessage('flash', [
            'message' => 'Your account has been created. Before you can log in, you must click the activation link we have sent to ' . $request->getParam('email'),
        ]);

        return $response->withRedirect($this->router->pathFor('auth.login'));
    }

    /**
     * Process account activation
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Message\ResponseInterface $response
     * @param array $args
     * @return mixed
     */
    public function getActivate(Request $request, Response $response, array $args)
    {
        $token = ActivationToken::where('token', $args['token'])->first();

        if (!$token) {
            $this->flash->addMessage('flash', [
                'message' => 'There is no account associated with this activation code.',
                'alert' => 'warning',
            ]);

            return $response->withRedirect($this->router->pathFor('home'));
        }

        if ($token->user->active) {
            $this->flash->addMessage('flash', [
                'message' => 'This account has already been activated.',
            ]);

            return $response->withRedirect($this->router->pathFor('home'));            
        }

        $token->user()->update([
            'active' => true,
        ]);

        $token->delete();

        $this->flash->addMessage('flash', [
            'message' => 'Your account is active and you can now log in!',
            'alert' => 'success',
        ]);

        return $response->withRedirect($this->router->pathFor('auth.login'));
    }

    /**
     * Process log out
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Message\ResponseInterface $response
     * @return mixed
     */
    public function getLogout(Request $request, Response $response)
    {
        $this->auth->logout();

        return $response->withRedirect($this->router->pathFor('home'));
    }
}
