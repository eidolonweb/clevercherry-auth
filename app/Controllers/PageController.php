<?php

namespace App\Controllers;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class PageController extends Controller
{
    /**
     * Renders administrator area view
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Message\ResponseInterface $response
     * @return mixed
     */
    public function getAdministratorArea(Request $request, Response $response)
    {
        return $this->view->render($response, 'areas/administrator.twig');
    }

    /**
     * Renders moderator area view
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Message\ResponseInterface $response
     * @return mixed
     */
    public function getModeratorArea(Request $request, Response $response)
    {
        return $this->view->render($response, 'areas/moderator.twig');
    }

    /**
     * Renders standard users' area view
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Message\ResponseInterface $response
     * @return mixed
     */
    public function getStandardArea(Request $request, Response $response)
    {
        return $this->view->render($response, 'areas/standard.twig');
    }
}