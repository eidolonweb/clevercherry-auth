<?php

namespace App\Controllers;

use Slim\Container;

abstract class Controller
{
    protected $container;
    protected $settings;

    public function __construct(Container $container)
    {
        $this->container = $container;
        
        $this->settings = [
            'mail' => $container->settings['mail'],         
        ];
    }

    public function __get($property)
    {
        if ($this->container->{$property}) {
            return $this->container->{$property};
        }

        return null;
    }
}