<?php

namespace App\Middleware;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class GuestMiddleware extends Middleware
{
    /**
     * Middleware responsible for redirecting authenticated users away from areas only accessible to guest users
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Message\ResponseInterface $response
     * @param callable $next
     * @return mixed
     */
    public function __invoke(Request $request, Response $response, callable $next)
    {
        if ($this->auth->check()) {
            $this->flash->addMessage('flash', [
                'message' => 'You must be logged out to view that page.',
                'alert' => 'warning',
            ]);

            return $response->withRedirect($this->router->pathFor('home'));
        }

        return $next($request, $response);
    }
}