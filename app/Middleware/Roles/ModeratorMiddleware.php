<?php

namespace App\Middleware\Roles;

use App\Middleware\Middleware;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class ModeratorMiddleware extends Middleware
{
    /**
     * Middleware responsible for redirecting users without Moderator or Administrator access away from the requested area.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Message\ResponseInterface $response
     * @param callable $next
     * @return mixed
     */
    public function __invoke(Request $request, Response $response, callable $next)
    {
        if (!$this->auth->user()->moderatorAccess()) {
            $this->flash->addMessage('flash', [
                'message' => 'That area is available only to Administrators and Moderators.',
            ]);

            return $response->withRedirect($this->router->pathFor('home'));
        }

        return $next($request, $response);
    }
}