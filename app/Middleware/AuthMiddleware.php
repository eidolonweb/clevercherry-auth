<?php

namespace App\Middleware;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class AuthMiddleware extends Middleware
{
    /**
     * Middleware responsible for redirecting guest users away from areas only accessible to authenticated users
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Message\ResponseInterface $response
     * @param callable $next
     * @return mixed
     */
    public function __invoke(Request $request, Response $response, callable $next)
    {
        if (!$this->auth->check()) {
            $this->flash->addMessage('flash', [
                'message' => 'You must log in to view this page.',
                'alert' => 'warning',
            ]);

            return $response->withRedirect($this->router->pathFor('auth.login'));
        }

        return $next($request, $response);
    }
}