<?php

namespace App\Middleware;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class AccountActiveMiddleware extends Middleware
{
    /**
     * Middleware responsible for redirecting authenticated inactive users away from areas only accessible to active users.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Message\ResponseInterface $response
     * @param callable $next
     * @return mixed
     */
    public function __invoke(Request $request, Response $response, callable $next)
    {
        if ($this->auth->check() && !$this->auth->user()->active) {
            $this->flash->addMessage('flash', [
                'message' => 'Your account is no longer active. Please contact Clevercherry to arrange for this account to be reactivated.',
            ]);

            $this->auth->logout();  

            return $response->withRedirect($this->router->pathFor('auth.login'));
        }

        return $next($request, $response);
    }
}