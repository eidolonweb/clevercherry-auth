<?php

use App\Middleware\AuthMiddleware;
use App\Middleware\GuestMiddleware;
use App\Middleware\AccountActiveMiddleware;
use App\Middleware\Roles\AdministratorMiddleware;
use App\Middleware\Roles\ModeratorMiddleware;

$app->get('/', 'HomeController:index')->setName('home')->add(new AccountActiveMiddleware($container));

/**
 * Authenticated users' routes
 */
$app->group('', function () use ($app, $container) {

	$app->get('/area/administrator', 'PageController:getAdministratorArea')
		->setName('area.admin')
		->add(new AdministratorMiddleware($container));

	$app->get('/area/moderator', 'PageController:getModeratorArea')
		->setName('area.moderator')
		->add(new ModeratorMiddleware($container));

	$app->get('/area/standard', 'PageController:getStandardArea')->setName('area.standard');

	$app->get('/logout', 'AuthController:getLogout')->setName('auth.logout');

})->add(new AuthMiddleware($container))
  ->add(new AccountActiveMiddleware($container));

/**
 * Guest users' routes
 */
$app->group('', function () use ($app) {

    $app->get('/login', 'AuthController:getLogin')->setName('auth.login');
    $app->post('/login', 'AuthController:postLogin');

    $app->get('/register', 'AuthController:getRegister')->setName('auth.register');
    $app->post('/register', 'AuthController:postRegister');

    $app->get('/activate/{token}', 'AuthController:getActivate')->setName('auth.activate');

})->add(new GuestMiddleware($container));
