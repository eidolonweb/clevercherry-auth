<?php

namespace App\Auth;

use Slim\Container;
use App\Models\User;

class Auth 
{
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function __get($property)
    {
        if ($this->container->{$property}) {
            return $this->container->{$property};
        }

        return null;
    }

    /**
     * Checks if user is logged in
     *
     * @return bool
     */
    public function check()
    {
        return isset($_SESSION['user']);
    }

    /**
     * Returns the authenticated user model stored in container
     *
     * @return \App\Models\User | null
     */
    public function user()
    {
        return $this->user;
    }

    /**
     * Attempts to log the user in
     *
     * @param string $email
     * @param string $password
     * @return int
     */
    public function attempt($email, $password)
    {
        $user = User::where('email', $email)->with('role')->first();

        if (!$user) {
            return false;
        }

        if (!password_verify($password, $user->password)) {
            return false;
        }

        $_SESSION['user'] = $user->id;
        return $user;
    }

    /**
     * Logs user out by destroying user session
     *
     * @return void
     */
    public function logout()
    {
        unset($_SESSION['user']);
    }
}