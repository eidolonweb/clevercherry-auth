<?php

$container = $app->getContainer();

/**
 * Register DI container components
 */

$container['view'] = function ($container) {
    $view = new Slim\Views\Twig(__DIR__ . '/../resources/views', [
        'cache' => false,
    ]);

    $view->addExtension(new Slim\Views\TwigExtension(
        $container->router,
        $container->request->getUri()
    ));

    // Attach authenticated user data to views
    $view->getEnvironment()->addGlobal('auth', [
        'check' => $container->auth->check(),
        'user' => $container->auth->user(),
    ]);

    // Attach error data
    if (isset($_SESSION['errors'])) {
        $view->getEnvironment()->addGlobal('errors', $_SESSION['errors']);
        unset($_SESSION['errors']);
    }

    // Attach old input data
    if (isset($_SESSION['old'])) {
        $view->getEnvironment()->addGlobal('old', $_SESSION['old']);
        unset($_SESSION['old']);
    }

    // Attach CSRF token data
    $view->getEnvironment()->addGlobal('csrf', [
        'keys' => [
            'name' => $container->csrf->getTokenNameKey(),
            'value' => $container->csrf->getTokenValueKey(),
        ],
        'name' => $container->csrf->getTokenName(),
        'value' => $container->csrf->getTokenValue(),
    ]);

    // Attach ReCaptcha settings
    $view->getEnvironment()->addGlobal('recaptcha', $container['settings']['recaptcha']);

    // Attach flash data
    if ($flash = $container->flash->getMessages('flash')) {
        $view->getEnvironment()->addGlobal('flash', $flash['flash']);
    }
    
    // Any other additional global variables for smaller data add here
    $view->getEnvironment()->addGlobal('global', [
        'time' => time(),
        'site' => $container['settings']['site'],
    ]);

    return $view;
};

$capsule = new Illuminate\Database\Capsule\Manager;

$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$container['db'] = function () use ($capsule) {
    return $capsule;
};

$container['mail'] = function () {
    return new PHPMailer;
};

$container['auth'] = function ($container) {
    return new App\Auth\Auth($container);
};

$container['user'] = function ($container) {
    if ($container->auth->check()) {
        return App\Models\User::with('role')->find($_SESSION['user']);
    }

    return null;
};

$container['csrf'] = function () {
    return new Slim\Csrf\Guard;
};

$container['flash'] = function () {
    return new Slim\Flash\Messages;
};

$container['random'] = function () {
    return new Rych\Random\Random;
};

$container['recaptcha'] = function ($container) {
    return new ReCaptcha\ReCaptcha(
        $container['settings']['recaptcha']['key']['secret'],
        new ReCaptcha\RequestMethod\CurlPost
    );
};

$container['validator'] = function ($container) {
    return new App\Validation\Validator($container);
};

Respect\Validation\Validator::with('App\\Validation\\Rules\\');

/**
 * Register controllers
 */

$container['HomeController'] = function ($container) {
    return new App\Controllers\HomeController($container);
};

$container['AuthController'] = function ($container) {
    return new App\Controllers\AuthController($container);
};

$container['PageController'] = function ($container) {
    return new App\Controllers\PageController($container);
};

/**
 * Register global middleware
 */
$app->add($container->csrf);
