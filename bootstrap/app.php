<?php

require __DIR__ . '/../vendor/autoload.php';

session_start();

$config = include __DIR__ . '/../app/config.php';

$app = new \Slim\App($config);

require __DIR__ . '/../app/container.php';

require __DIR__ . '/../app/routes.php';

$app->run();
